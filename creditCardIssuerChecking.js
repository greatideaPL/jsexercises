//Credit card issuer checking
//7 kyu

/*
Description:

Given a credit card number we can determine who the issuer/vendor is with a few basic knowns.

+============+=============+===============+
| Card Type  | Begins With | Number Length |
+============+=============+===============+
| AMEX       | 34 or 37    | 15            |
+------------+-------------+---------------+
| Discover   | 6011        | 16            |
+------------+-------------+---------------+
| Mastercard | 51-55       | 16            |
+------------+-------------+---------------+
| VISA       | 4           | 13 or 16      |
+------------+-------------+---------------+

Write a function (getIssuer(number) (get_issuer(number) for Python)) that will use the above known values to determine the card issuer given a card number. If the number cannot be matched then the function should return the string Unknown.

Some sample numbers and their issuer:

getIssuer(4111111111111111) == "VISA"
getIssuer(4111111111111) == "VISA"
getIssuer(4012888888881881) == "VISA"
getIssuer(378282246310005) == "AMEX"
getIssuer(6011111111111117) == "Discover"
getIssuer(5105105105105100) == "Mastercard"
getIssuer(5105105105105106) == "Mastercard"
getIssuer(9111111111111111) == "Unknown"

*/


function getIssuer(number) {
    // Code your solution here
    if (number >= 5100000000000000 && number < 5600000000000000) {
        return "Mastercard"
    }
    var sNumber = number.toString();
    if (sNumber.slice(0, 1) === "4" && sNumber.length > 12 && sNumber.length < 17) {
        return "VISA"
    }
    else if ((sNumber.slice(0, 2) === "34" || sNumber.slice(0, 2) === "37") && (sNumber.length === 15)) {
        return "AMEX"
    }
    else if (sNumber.slice(0, 4) === "6011" && sNumber.length === 16) {
        return "Discover"
    }
    else return "Unknown";
}